<?php

namespace Drupal\simple_date_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Datetime\DateTimePlus;

/**
 * Provides a 'Simple Date Block'
 *
* @Block(
 *   id = "simple_date_block",
 *   admin_label = @Translation("Simple Date block"),
 * )
 */
class SimpleDateBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $simple_date_time = DateTimePlus::createFromTimestamp(strtotime('now'));
    $simple_date_format = $config['simple_date_block_format'] ?? 'd/m/Y';
    $simple_date = $simple_date_time->format($simple_date_format);

    return [
      '#markup' => $simple_date,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();
    $form['simple_date_block_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Date format"),
      '#description' => $this->t('Please provide the date format you want to display the date in. Please refer to <a href =":link">Date format</a> for more details about the date format', [':link' => 'https://www.php.net/manual/en/datetime.format.php']),
      '#default_value' => $config['simple_date_block_format'] ?? 'd/m/Y',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('simple_date_block_format', $form_state->getValue('simple_date_block_format'));
  }
}
